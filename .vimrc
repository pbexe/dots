call plug#begin('~/.vim/plugged')
Plug 'vim-airline/vim-airline'
Plug 'scrooloose/nerdtree'
Plug 'ryanoasis/vim-devicons'
Plug 'morhetz/gruvbox'
Plug 'w0rp/ale'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
Plug 'ferrine/md-img-paste.vim'
Plug 'dylanaraps/wal.vim'
call plug#end()
map <C-n> :NERDTreeToggle<CR>
set encoding=UTF-8
colorscheme wal
set background=dark
set number
let g:vim_markdown_math = 1
:set tabstop=8
:set expandtab
:set shiftwidth=4
:set autoindent
:set smartindent
:set cindent     
autocmd FileType markdown nmap <silent> <leader>p :call mdip#MarkdownClipboardImage()<CR>

